# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 Solucións Aloxa S.L. <info@aloxa.eu>
#        Alexandre Díaz <alex@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    def get_origin_sale_order(self):
        if not self.origin:
            return False
        return self.env['sale.order'].search([('name', '=', self.origin)], limit=1)
    
    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        res = super(stock_picking, self)._get_invoice_vals(cr, uid, key, inv_type, journal_id, move, context=context)
        if move and move.picking_id:
            origin_sale_order = move.picking_id.get_origin_sale_order()
            if origin_sale_order and origin_sale_order.partner_invoice_delivery_address_id:
                res['address_shipping_id'] = origin_sale_order.partner_invoice_delivery_address_id.id
        return res
    