# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 Solucións Aloxa S.L. <info@aloxa.eu>
#        Alexandre Díaz <alex@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields

class res_partner(models.Model):
    _inherit = 'res.partner'

    type = fields.Selection(selection_add=[('invoice_delivery_address', 'Invoice Delivery Address')])
    
    
    def get_invoice_delivery_address(self):
        addr = self.address_get(['invoice_delivery_address'])
        if 'invoice_delivery_address' in addr and addr['invoice_delivery_address']:
            return addr['invoice_delivery_address']
        else:
            return False
    