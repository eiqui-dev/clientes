# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 Solucións Aloxa S.L. <info@aloxa.eu>
#        Alexandre Díaz <alex@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
{
    'name': "Invoice Delivery Address",
    'summary': "Adds a field for select 'Invoice Delivery Address' in sale.order and stock.picking",
    'description': """
        Adds a field for select 'Invoice Delivery Address' in sale.order and stock.picking.
        Adds a new contact type 'Invoice Delivery Address'
    """,
    'author': "Solucións Aloxa S.L.",
    'website': "http://eiqui.com",
    'category': 'Warehouse',
    "icon": "/aloxa_dolmar_direccion_envio_factura/static/src/img/icon.png",    
    'version': '0.1',
    'depends': ['base', 'sale', 'account', 'stock', 'account_invoice_shipping_address'],
    'data': [
        'views/inherit_sale_order.xml',
    ],
    "installable": True,
}