# -*- coding: utf-8 -*-

from openerp import models, api, fields, exceptions
from datetime import datetime
from openerp.addons.website.models.website import slugify

class dolmar_directorio_website(models.Model):
    _inherit = 'website'
    
    def get_product_attachments(self, id):
        attachments = self.env['ir.attachment'].search(['&',('res_model', '=', 'product.template'), ('res_id', '=', id)])
        return attachments
    
    
class dolmar_directorio_attachments(models.Model):
    _inherit = 'ir.attachment'
    _name = 'ir.attachment'
    
    #Fields
    dolmar_type = fields.Integer()
    
    
class dolmar_directorio_product_template(models.Model):
    _inherit = 'product.template'
    _name = 'product.template'
    
    #Fields
    directory_ok = fields.Boolean('Mostrar en Directorio')
    
    
class dolmar_directorio_product_public_category(models.Model):
    _name = 'product.public.category'
    _inherit = 'product.public.category'
    
    #Fields
    directory_ok = fields.Boolean('Mostrar en Directorio')