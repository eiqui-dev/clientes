# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
{
    'name': "DOLMAR - Sitio Web",

    'summary': """
        Módulo para habilitar nuevas funcionalidades al sitio web""",

    'description': """
        Crea una página web llamada directorio para presentar los productos y sus correspondientes documentos por categorias. 
    """,

    'author': "Solucions Aloxa S.L.",
    'website': "http://www.aloxa.eu",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Theme/Creative',
    "icon": "/aloxa_dolmar_directorio/static/src/img/icon.png",    
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web','website_partner','kingfisher','knowledge'],

    #QWeb
    'qweb':['static/src/base.xml'],
    
    #JS
    'js':['static/src/js/module.js'],

    # always loaded
    'data': [        
        'views/general_templates.xml',
        'views/producto_template.xml',
        'views/directorio_productos_template.xml',
        'views/directorio_novedades_template.xml'
        #'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [        
    ],
}