  # -*- coding: utf-8 -*-

from openerp import http
from datetime import datetime
from openerp.addons.web.http import Controller, route, request
from openerp.addons.web.controllers.main import serialize_exception
from openerp.tools import html_escape
import simplejson, base64
#import pydevd

class QueryURL(object):
    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k,v in self.args.items():
            kw.setdefault(k,v)
        l = []
        for k,v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k,i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k,v)]))
        if l:
            path += '?' + '&'.join(l)
        return path
    

class website_aloxa_turismo(http.Controller):    
    @http.route(['/producto/<model("product.template"):product>'], type='http', auth="public", website=True)
    def producto(self, product):
        values = {
            'product': product
        }
        return request.website.render("aloxa_dolmar_directorio.product_details", values)
    
    
    @http.route([
                 '/productos',
                 '/productos/categoria/<model("product.public.category"):category>',
                 ], type='http', auth="public", methods=['GET'], website=True)
    def directorio_productos(self, category=None, **params):
        #pydevd.settrace("10.0.3.1")
        products = []
        categories = request.env['product.public.category'].search([('parent_id', '=', False),('directory_ok', '=', True)])
        
        searchDomain = [('product_tmpl_id.sale_ok', '=', True),('product_tmpl_id.directory_ok', '=', True)]
        if category:
            searchDomain.append(('public_categ_ids', 'in', category.id))
            products = request.env['product.product'].search(searchDomain)
            
        attrib_list = request.httprequest.args.getlist('attrib')
        keep = QueryURL('/productos', category=category and int(category), search='', attrib=attrib_list)
        values = {
            'category': category,
            'categories': categories,
            'keep': keep,
            'products': products,
        }
        return request.website.render("aloxa_dolmar_directorio.directory_products", values)
    
    
    @http.route(['/novedades'], type='http', auth="public", methods=['GET'], website=True)
    def directorio_novedades(self, **params):        
        products = request.env['product.product'].search([('product_tmpl_id.sale_ok', '=', True),('product_tmpl_id.directory_ok', '=', True),('product_tmpl_id.is_arrival', '=', True)])   
        values = {
            'products': products,
        }
        return request.website.render("aloxa_dolmar_directorio.directory_developments", values)
    
    
    @http.route([
                 '/web/binary/upload_attachment_dolmar',
                 ], type='http', auth="user")
    @serialize_exception
    def upload_attachment_dolmar(self, callback, model, id, ufile, dolmar_type = 0):        
        Model = request.session.model('ir.attachment')
        out = """<script language="javascript" type="text/javascript">
                    var win = window.top.window;
                    win.jQuery(win).trigger(%s, %s);
                </script>"""
        try:
            #pydevd.settrace("10.0.3.1")
            attachment_id = Model.create({
                'name': ufile.filename,
                'datas': base64.encodestring(ufile.read()),
                'datas_fname': ufile.filename,
                'res_model': model,
                'res_id': int(id),
                'dolmar_type': dolmar_type
            }, request.context)
            args = {
                'filename': ufile.filename,
                'id':  attachment_id,
                'dolmar_type': dolmar_type
            }
        except Exception:
            args = {'error': "Something horrible happened"}
            _logger.exception("Fail to upload attachment %s" % ufile.filename)
        return out % (simplejson.dumps(callback), simplejson.dumps(args))
