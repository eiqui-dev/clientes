openerp.aloxa_dolmar_directorio = function (instance) {

    _t = instance.web._t;
    instance.web.Sidebar.include({
		do_attachement_update: function(dataset, model_id, args) {
		    var self = this;

		    this.dataset = dataset;
		    this.model_id = model_id;
		    if (args && args[0].error) {
		        this.do_warn(_t('Uploading Error'), args[0].error);
		    }
		    if (!model_id) {
		        this.on_attachments_loaded([]);
		    } else {
		        var dom = [ ['res_model', '=', dataset.model], ['res_id', '=', model_id], ['type', 'in', ['binary', 'url']] ];
		        var ds = new instance.web.DataSetSearch(this, 'ir.attachment', dataset.get_context(), dom);
		        ds.read_slice(['name', 'url', 'type', 'create_uid', 'create_date', 'write_uid', 'write_date', 'dolmar_type'], {}).done(this.on_attachments_loaded);
		    }
		},
	    on_attachment_changed: function(e) {
	        var $e = $(e.target);
	        if ($e.val() !== '') {
	        	var $form = $e.parent();
	        	$form.submit();
	        	$form.find('input[type=file]').prop('disabled', true);
	        	$form.find('button').prop('disabled', true).find('img, span').toggle();
	        	$form.find('.oe_sidebar_add_attachment span').text(_t('Uploading...'));
	            instance.web.blockUI();
	        }
	    }
    });
    
};
