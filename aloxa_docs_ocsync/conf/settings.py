# -*- coding: utf-8 -*-
#Ruta al filestore de Odoo
ODOO_FILESTORE="/opt/frikidoo/.local/share/Odoo/filestore"
#Frecuencia con la que se ejecuta la sincronizacion Owncloud->Odoo
SYNC_ACTION_MINUTES=1
