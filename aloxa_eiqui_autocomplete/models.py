# -*- coding: utf-8 -*-
from openerp import models, fields, api
#import pydevd
import re

class res_partner_bank(models.Model):
    _inherit = 'res.partner.bank'
    
    @api.model
    def complete_bank_inf(self):
        #pydevd.settrace('192.168.3.1')
        res_partner_banks = self.search([])
        for rpb in res_partner_banks:
            iban = rpb.acc_number or False
            bank_name = bank_bic = ""
            if iban:
                iban = re.sub(r'\s+', '', rpb.acc_number)
                pais_cod = iban[0:2] if len(iban) == 24 else False
                bank_cod = iban[4:8] if len(iban) == 24 else False
                country = self.env['res.country'].search([('code','=',pais_cod)]) if pais_cod else False
                if country and bank_cod:
                    #bank = self.env['res.bank'].search(['&',('code','=',bank_cod),('country','=',country.id)])
                    banks = self.env['res.bank'].search([])
                    bank = banks.filtered(lambda r: r.code == bank_cod and r.country.id == country.id)                                                            
                    bank_name = bank.name or False
                    bank_bic = bank.bic or False            
            owner_name = rpb.partner_id.name or False
            street = rpb.partner_id.street or False
            city = rpb.partner_id.city or False
            zip = rpb.partner_id.zip or False
            country_id = rpb.partner_id.country_id.id or False
            state_id = rpb.partner_id.state_id.id or False
                
            rpb.write({'bank_name':bank_name,
                       'bank_bic':bank_bic,
                       'bank':bank.id if bank else False,
                       'owner_name':owner_name,
                       'street':street,
                       'city':city,
                       'zip':zip,
                       'country_id':country_id,
                       'state_id':state_id})

    #===========================================================================
    # def _get_bank_name(self):
    #     pydevd.settrace('192.168.3.1')
    #     if self.bank:
    #         return self.env['res.bank'].browse(self.bank.id).name
    # 
    # def _get_bank_bic(self):
    #     pydevd.settrace('192.168.3.1')
    #     if self.bank:
    #         return self.env['res.bank'].browse(self.bank.id).bic
    # 
    # def _get_street(self):
    #     pydevd.settrace('192.168.3.1')
    #     if self.partner_id:
    #         return self.env['res.partner'].browse(self.partner_id.id).street
    #     
    # def _get_owner_name(self):
    #     pydevd.settrace('192.168.3.1')
    #     if self.partner_id:
    #         return self.env['res.partner'].browse(self.partner_id.id).name       
    #  
    # #fields
    # bank_name = fields.Char(default=_get_bank_name)
    # bank_bic= fields.Char(default=_get_bank_bic)
    # street = fields.Char(default=_get_street)
    # owner_name = fields.Char(default=_get_owner_name)
    #===========================================================================
     
res_partner_bank()
