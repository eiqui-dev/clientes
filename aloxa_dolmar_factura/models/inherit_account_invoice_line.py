#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api
from datetime import datetime, timedelta
from openerp.addons.product import product as prodm

'''
Modelo que sobreescribe facturas 
'''
class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"
    
    @api.one
    def load_line_lots(self):
        super(account.invoice.line, self).load_line_lots()
        if self.prod_lot_ids:
            note = u' '.join([lot.name for lot in self.prod_lot_ids])        
            self.lot_formatted_note = note