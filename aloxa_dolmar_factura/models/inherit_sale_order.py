# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api
from datetime import datetime, timedelta
from openerp.addons.product import product as prodm

'''
Modelo que sobreescribe sale.order (Pedidos, Presupuestos)

'''
class sale_order(models.Model):
    _name='sale.order'
    _inherit='sale.order'
    
    @api.multi
    def total_discount(self):
        total = 0
        for res in self:
            for line in res.order_line:
                if line.product_uom_qty and line.price_unit:
                    total+=line.product_uom_qty * line.price_unit
            return total
    
            
        
            
    
    