# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields

'''
Modelo que extiende el producto e incluye la lista de muestra los precios de la competencia
'''
class product_template_dolmar(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'
    #fields 
    precios_competencia = fields.One2many('dolmar.competencia', 'product_id', 'Precios Competencia')
    royalties = fields.Boolean('Royalties')