# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields

'''
Modelo que define los precios de la competencia
'''
class dolmar_competencia(models.Model):                       
    _name = 'dolmar.competencia'        
    #Fields               
    precio = fields.Float('Precio')
    product_id = fields.Many2one('product.template', 'Producto')
    competencia = fields.Char('Empresa', size=255)
    zona = fields.Many2many('dolmar.zona.tag', string='Zona')
    tipo_cliente = fields.Many2many('dolmar.tipo.cliente.tag', string='Tipo Cliente')
    fecha = fields.Date('Fecha')
    comentarios = fields.Char('Comentarios')