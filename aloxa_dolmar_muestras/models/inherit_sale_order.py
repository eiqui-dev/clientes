# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api

'''
Modelo que sobreescribe sale.order (Pedidos, Presupuestos)
que agrega el campo que enlaza con la Oportunidad (campo calculado)
'''
class crm_sale_order_dolmar(models.Model):
    _name='sale.order'
    _inherit='sale.order'
    
    @api.multi
    def obtener_oport(self):
        for record in self:
            #pydevd.settrace("10.0.3.1")
            if record.origin:
                lead_id = record.origin.split(':')[1].strip()
                crm_lead = self.env['crm.lead'].browse([lead_id])
                if crm_lead:
                    record.lead_id = crm_lead.id
                
    @api.model
    def _prepare_invoice(self, order, lines):
        """Copy text proforma from partner to invoice"""
        vals = super(crm_sale_order_dolmar, self)._prepare_invoice(order, lines)
        if order.partner_id:
            vals['texto_inv_proforma'] = order.partner_id.texto_proforma
        return vals
    
    #FIELDS
    lead_id = fields.Many2one('crm.lead', compute='obtener_oport', string='Oportunidad')