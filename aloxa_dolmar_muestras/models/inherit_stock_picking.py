# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api

'''
Hacer editable el cliente en los albaranes una vez transferidos
'''        
class stock_picking(models.Model):
    _inherit = 'stock.picking'
    
    @api.model
    def _prepare_invoice(self, picking, partner,
                     inv_type, journal_id, context=None):
        """Copy text proforma from partner to invoice"""
        invoice_vals = super(stock_picking, self)._prepare_invoice(
            picking, partner, inv_type, journal_id, context=context)
        if picking.sale_id and picking.sale_id.partner_id:
            invoice_vals['texto_inv_proforma'] = (
                picking.sale_id.partner_id.texto_proforma)
        return invoice_vals

    #FIELDS

    partner_id = fields.Many2one(states={'cancel': [('readonly', True)]})