# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields

'''
Modelo que define una muestra
'''
class dolmar_muestra(models.Model):                       
    _name = 'dolmar.muestra'    
            
    #Fields               
    tipo_entrega = fields.Selection([('Mensajeria', 'Mensajería'), ('Mano', 'En Mano')], 'Entrega')  
    #crear_aviso = fields.Boolean('Aviso', default=False)
    probada = fields.Boolean('Probada')
    motivo_no_probada = fields.Many2one('dolmar.motivos.probada', 'Motivo no probada')
    satisfaccion = fields.Boolean('Éxito')
    motivo_no_satisfaccion = fields.Many2one('dolmar.motivos.satisf', 'Motivo no satisfacción')
    tipo_presentacion = fields.Many2one('dolmar.tipos.present', 'Presentación')
    #Una muestra tiene un producto asociado
    product_id = fields.Many2one('product.product', 'Producto Asociado')
    lead_id = fields.Many2one('crm.lead', 'Oportunidad', readonly='True')