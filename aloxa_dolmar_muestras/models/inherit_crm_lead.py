# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api
from datetime import datetime, timedelta

'''
Modelo que extiende la oportunidad y incluye la lista de muestras asociadas a la misma
'''
class crm_lead_dolmar(models.Model):
    _name = 'crm.lead'
    _inherit = 'crm.lead'
    
    @api.multi
    def write(self, values):
        #pydevd.settrace('192.168.3.1')
        lead = super(crm_lead_dolmar, self).write(values)        
        #Si la casilla de crear aviso esta marcada
        if self.crear_aviso:
            #Deshabilitamos la marca de crear aviso
            self.crear_aviso = False                   
            #logica de creacion del evento
            irconfig_par_model = self.env['ir.config_parameter']        
            #Buscamos si esta definido un parametro del sistema para el num dias del aviso
            pnumdias = irconfig_par_model.search([('key', '=', 'muestras.dias.aviso')])           
             #Sino existe el parametro se crea con un valor por defecto           
            if not pnumdias.value:
               pnumdias = irconfig_par_model.create({'key':'muestras.dias.aviso', 'value':'5'})                
            cal_model = self.env['calendar.event']
            #Borramos posible aviso de calendario anterior asociados a la oportunidad
            for event in cal_model.search([('opportunity_id','=',self.id)]):
                event.unlink()
            #Creamos el nuevo aviso en el calendario
            cal_event = cal_model.create({                   
                    'name':'Aviso: ' + self.name + ', ' +\
                            unicode(self.partner_id.name if self.partner_id else ''),                            
                    'opportunity_id' : self.id,
                    'class': 'private',                    
                    'user_id' : self.env.user.id,                                                                   
                    'state' : 'open',
                    'message_unread': True,
                    'start' :fields.Datetime.now(),
                    'stop' : fields.Datetime.now(),
                    })
            #cal_event.partner_ids += self.env['res.partner'].search([('id','=',self.env.user.partner_id.id)])
            #Definimos los instantes de inicio y fin del evento de calendario
            cal_event.stop = datetime.now() + timedelta(days=int(pnumdias.value)) 
            cal_event.start = datetime.strptime(cal_event.stop, '%Y-%m-%d %H:%M:%S') + timedelta(seconds=-1)                                               
            #Establecimiento de la alarma de aviso asociada al evento del calendario
            cal_alarm_model = self.env['calendar.alarm']                                          
            newalarm = cal_alarm_model.search([('name','like','1 day notif')])
            cal_event.alarm_ids = newalarm
    
        return lead

    '''
    Obtiene el presupuesto a partir de la Oportunidad (si existe)
    '''
    @api.multi
    def obtener_presu(self):
        self.ensure_one()
#        pydevd.settrace("10.0.3.1")        
        sale_order = self.env['sale.order'].search([('origin','like',': '+str(self.id))])
        if sale_order:
            self.presupuesto_id = sale_order.id
                
                    
    #FIELDS
    #Una oportunidad tiene asociadas varias muestras
    muestra_ids = fields.One2many('dolmar.muestra', 'lead_id', 'Muestras')
    #Si se crea un presupuesto para la oportunidad lo
    presupuesto_id = fields.Many2one('sale.order', compute='obtener_presu', string='Presupuesto')
    crear_aviso = fields.Boolean('Crear Aviso', default=False)