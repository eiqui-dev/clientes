# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api

class stock_move(models.Model):
    _inherit = 'stock.move'
    
    @api.multi
    def action_move_description(self):
        descrip_form = self.env.ref('aloxa_dolmar_muestras.dolmar_description_move', False)
        return {
            'name': 'Change Description',
            'type': 'ir.actions.act_window',
            'res_model': 'stock.move',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(descrip_form.id, 'form')],
            'view_id': descrip_form.id,
            'res_id': self.id,
            'flags': {'form':{'action_buttons': True, 'options':{'mode':'edit'}}},
        }