# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api

'''
Modelo que sobreescribe el modelo sale.order.line de las lineas de pedido/presupuesto
para agregar el campo de Precio Anterior
'''
class sale_order_line_dolmar(models.Model):
    _name='sale.order.line'
    _inherit='sale.order.line'
    
    #Obtiene el ultimo precio ofertado para el producto al cliente
    @api.multi
    def obtener_precio_anterior(self):
        for record in self:        
            producto = record.product_id
            cliente = record.order_id.partner_id
            
            if producto and cliente:
                records = self.search(['&', ('product_id.id', '=', producto.id),
                                     ('order_id.partner_id.id', '=', cliente.id)])
                if records:
                    tam = len(records)
                    if tam > 1:
                        record.precio_anterior = records[tam -2].price_unit               
                    else:
                        record.precio_anterior = records[0].price_unit
    
    #FIELDS
    precio_anterior = fields.Float(compute='obtener_precio_anterior', string='Prec Ant')