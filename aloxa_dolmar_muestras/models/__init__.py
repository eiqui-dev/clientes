# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from . import dolmar_motivos_probada
from . import dolmar_motivos_satisf
from . import dolmar_muestra
from . import dolmar_tipos_present
from . import inherit_account_invoice
from . import inherit_calendar_event
from . import inherit_crm_lead
from . import inherit_crm_make_sale
from . import inherit_product_product
from . import inherit_product_template
from . import inherit_res_company
from . import inherit_res_partner
from . import dolmar_competencia
from . import inherit_sale_order_line
from . import inherit_sale_order
from . import inherit_stock_move
from . import inherit_stock_picking
from . import tags