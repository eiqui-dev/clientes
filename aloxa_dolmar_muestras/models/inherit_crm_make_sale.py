# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp import models, fields, api


'''
Modelo para sobreescribir el metodo que crear un Presupuesto a partir
de una Oportunidad
'''
class crm_make_sale_dolmar(models.Model):
    _name = 'crm.make.sale'
    _inherit = 'crm.make.sale'
    
    @api.multi
    def makeOrder(self):
        #pydevd.settrace("10.0.3.1")
        value = super(crm_make_sale_dolmar, self).makeOrder()        
        #El campo res_id contiene el id del sale_order (Presupuesto)
        if 'res_id' in value and value['res_id']:
            sale_order_id = value['res_id']
            sale_order = self.env['sale.order'].browse([sale_order_id])       
            #Obtenemos el id de Oportunidad
            opportunity_id = int(sale_order.origin.split(':')[1].strip())           
            #Tomamos las muestras
            muestras = self.env['dolmar.muestra'].search([('lead_id.id', '=', opportunity_id)])
            lines = list()
            for muestra in muestras:
                #Para cada muestra de la Oportunidad se crea una linea de presupuesto                
                if muestra.product_id:
                    vals = {'order_id' : sale_order_id, 'product_id' : muestra.product_id.id}
                    sale_order_line = self.env['sale.order.line'].create(vals)
                    lines.append(sale_order_line.id)                       
            
            sale_order.order_line = lines 
        #Invocamos a la vista formulario de sale.order para ver el recien creado        
        return  {
            'type': 'ir.actions.act_window',
            'name': 'sale.order.form',
            'res_model': 'sale.order',
            'res_id': value['res_id'], #If you want to go on perticuler record then you can use res_id 
            'view_type': 'form',
            'view_mode': 'form',            
            'target': 'current',
            'nodestroy': True,
        }