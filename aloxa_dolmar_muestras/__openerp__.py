# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################


{
    "name": "Módulo de Gestión de Muestras para Dolmar",
    "version": "0.1",
    "category": "",
    "icon": "/aloxa_dolmar_muestras/static/src/img/icon.png",
    "depends": [
                'base',
                'product',
                'crm',
                'sale_crm',
                'stock',
                ],
    "author": "Solucións Aloxa S.L.",
    "description": "Módulo de Gestión de Muestras para Dolmar",
    "init_xml": [],
    "data": [
            'views/dolmar_competencia.xml',
            'views/dolmar_motivos_satisf.xml',
            'views/dolmar_muestra.xml',
            'views/dolmar_tipos_present.xml',
            'views/inherit_account_invoice_form.xml',
            'views/inherit_base_view_company_form.xml',
            'views/inherit_base_view_partner_form.xml',
            'views/inherit_calendar_view_calendar_event_form_popup.xml',
            'views/inherit_crm_crm_case_form_view_oppor.xml',
            'views/inherit_product_product_normal_view_form_view.xml',
            'views/inherit_product_product_template_only_form_view.xml',
            'views/inherit_sale_view_order_form.xml',
            'views/inherit_stock_view_move_picking_form.xml',
            'views/inherit_stock_view_picking_internal_search.xml',
            'views/stock_move.xml',
            'views/actions.xml',
            'views/menus.xml',
            
            'security/ir.model.access.csv',
            ],
    "demo_xml": [],
    "installable": True,
    "active": False,
}
