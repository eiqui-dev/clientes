# -*- coding: utf-8 -*-
{
    'name': "eiqui_registro",

    'summary': """
        Registro de Clientes de eiqui.com""",

    'description': """
        Registro de Clientes de eiqui.com
    """,

    'author': "Solucions Aloxa S.L.",
    'website': "http://www.aloxa.eu",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'eiqui.com',
    "icon": "/aloxa_eiqui_registro/static/src/img/icon.png",    
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [        
    ],
}