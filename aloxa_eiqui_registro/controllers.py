# -*- coding: utf-8 -*-
from openerp import http
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from werkzeug.utils import redirect
from random import randint
import smtplib
import hashlib
import time
import requests

#import pydevd

class Registro(http.Controller):

    #handler para le entrada al servicio de validacion
    @http.route('/eiqui/entrar', auth='public', website=True)
    def entrar(self, **kw):
        #pydevd.settrace("10.0.3.1")
        domain = kw['orgdom']
        response = http.request.render('aloxa_eiqui_registro.entrar')
        #Crear la cookie que identifica el dominio eiqui del cliente que se envia al cliente
        response.set_cookie('orgdom', domain, domain="registro.eiqui.com")        
        return response

    #handler para validacion del mail introducido en el formulario
    @http.route('/eiqui/validar', auth='public', website=True)
    def validar(self, **kw):        
         #pydevd.settrace("10.0.3.1")
         #Tomo la cookie que identifica el dominio eiqui del cliente
         request = http.request
         if 'orgdmon' in request.httprequest.cookies:
             orgdom = request.httprequest.cookies['orgdom']
             #Validacion del recaptcha
             recaptcha_res = kw['g-recaptcha-response']
             payload = {'secret': '6LcXxgITAAAAACUdyldwgjfTQd75GBYumco-ob3g', 'response': recaptcha_res}
             #Cotejamos con el API de Google la validacion
             res = requests.post("https://www.google.com/recaptcha/api/siteverify", data=payload)
             if hasattr(res, 'json'):
                 success = res.json['success']         
             else:
                 success = res.json()['success']
             if not success:
                 return http.request.render('aloxa_eiqui_registro.entrar')
             #Tras la validacion del recaptcha validamos el mail del cliente
             mail = ""
             if kw and kw['Mail']:
                 mail = kw['Mail']
                 #Obtenemos el res_partner con el mail recibido         
                 res_partner_rs = http.request.env['res.partner']             
                 el_res_partner = res_partner_rs.sudo().search([('email', '=', mail)], limit=1)
                 #Si existe ese res_partner construimos el enlace para enviar al mail
                 if el_res_partner:
                     #Creamos un hash del dominoi eiqui del cliente (cotejamiento posterior)                 
                     vhash = hashlib.sha224(orgdom).hexdigest()
                     #Pasamos el instante actual (evitar duplicados caducados)
                     vtime = int(round(time.time())*1000)
                     sendMail(mail, '?origen=' + mail +
                                    '&time=' + str(vtime) + '&hash=' + vhash)
    
                     return http.request.render('aloxa_eiqui_registro.registro_ok')
         #Si algo va mal devuelve una repuesta NO OK
         return http.request.render('aloxa_eiqui_registro.registro_nok')             
        
    #handler para confirmacion del enlace enviado al mail del cliente
    @http.route('/eiqui/confirmar/', auth='public', website=True)
    def confirmar(self, **kw):        
        #pydevd.settrace("10.0.3.1")
        request = http.request
        #Comprobacion de la URL
        if 'orgdmon' in request.httprequest.cookies and kw['origen'] and kw['time'] and kw['hash']:
            orgdom = request.httprequest.cookies['orgdom']
            mail = kw['origen']
            vtime = kw['time']
            vhash = kw['hash']            
            vtime = int(vtime)
            #La comprobacion solo sera valida los 15 minutos siguientes
            if int(round(time.time())*1000) - vtime < 15*60*1000:
                #Obtenemos el res_partner asociado al email                      
                res_partner_rs = http.request.env['res.partner']
                el_res_partner = res_partner_rs.sudo().search([('email', '=', mail)], limit=1)
                if el_res_partner:
                    #pydevd.settrace("10.0.3.1")
                    #Comprobamos que el hash es correcto
                    new_hash = hashlib.sha224(orgdom).hexdigest()
                    if new_hash == vhash:                        
                        response = http.request.render('aloxa_eiqui_registro.registro_validado')
                        #Crear la cookie para enviar al cliente
                        response.set_cookie('eiqui_id', vhash, max_age=20*365*24*3600, domain=".eiqui.com")                        
                        return response
        return http.request.render('aloxa_eiqui_registro.algova_mal')
    
    #handler para confirmacion del cliente que envia la cookie
    @http.route('/eiqui/cliente/', auth='public', website=True)
    def cliente(self, **kw):
        if kw['host'] and kw['db']:
            host = kw['host']
            db = kw['db']
            #Obtenemos la cookie para identificacion
            if http.request.httprequest.cookies['eiqui_id']:
                eiqui_id = http.request.httprequest.cookies['eiqui_id']
                #pydevd.settrace("10.0.3.1")
                #Cotejamos la cookie con el hash sobre el host (dominio) del cliente
                if eiqui_id == hashlib.sha224(host).hexdigest():                
                    #Si coincide reenviamos al servidor eiqui del cliente
                    response = redirect('http://' + host + '/web/?db=' + db)                    
                    #Establecemos otra cookie para distinguir en el proxy aquellas peticiones ya validadas
                    response.set_cookie('eiqui_ticket', str(randint(1,100000)), max_age=2*3600, domain=".eiqui.com")
                    return response
        return http.request.render('aloxa_eiqui_registro.autorizado_no')
                        
#Funcion para envio de mails
def sendMail(receiver, id):
    try:
        msg = MIMEMultipart()
        msg['From'] = 'info@aloxa.eu'
        msg['To'] = receiver
        msg['Subject'] = 'Bienvenido a eiqui.com'
        message = 'Haz click en el siguiente enlace para verificar tu identidad: ' +\
                  'http://registro.eiqui.com/eiqui/confirmar/' + id +\
                  '\nSi algo va mal copia y pega el texto del enlace directamente en tu navegador web'
        msg.attach(MIMEText(message))
        
        mailserver = smtplib.SMTP('mail.aloxa.eu',587)
        # identify ourselves to smtp gmail client
        mailserver.ehlo()
        # secure our email with tls encryption
        mailserver.starttls()
        # re-identify ourselves as an encrypted connection
        mailserver.ehlo()
        mailserver.login('incidencias@aloxa.eu', '@bc123.')
        
        mailserver.sendmail(msg['From'], receiver, msg.as_string())
        
        mailserver.quit()
    except Exception:
       print "Error: unable to send email"