'''
Este codigo debe ser incorporado en el archivo
 
addons.web.controllers.main.py

dentro de la clase Home (linea 466 aproximadamente)

El objetivo es crear un metodo de controller para preseleccionar la base de datos del cliente
en la zona de acceso publico.
El proxy redirigira la peticion desde la pagina principal a la URL del controlador correspondiente (/seldb)
con el parametro ?db=bbdd establecido
'''
import openerp.addons.web.controllers.main as main

from openerp.addons.web import http
from openerp.addons.web.http import request

class Home(main.Home):
    
    ###ALOXA.EU...PRESELECCION DDBB
    @http.route('/seldb', type='http', auth="none")
    def preselectDB(self, **kw):
        if request.params.get('db'):
            database = request.params.get('db')
            #Metemos el nombre de base de datos en la sesion
            request.session.db = database
            #Redirigimos a la URL principal            
            return http.local_redirect('/', query=request.params, keep_hash=True)

