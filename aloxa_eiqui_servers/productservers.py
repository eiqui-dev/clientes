# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
import re

#import pydevd

class product_server(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'
    
    #fields
    eiqui_server_is_server = fields.Boolean('Servidor eiqui.com', default=False)
    eiqui_server_hostname = fields.Char('Hostname', size=80)
    eiqui_server_ip_privada = fields.Char('IP Privada', size=15)
    eiqui_server_ip_publica = fields.Char('IP Pública', size=15)
    eiqui_server_droplet_url = fields.Char('URL Droplet', size=100)
    eiqui_server_doc_url = fields.Char('URL Documentación', size=100)
    eiqui_server_descripcion = fields.Text('Descripcion')
    eiqui_server_cpu = fields.Selection([('1','1'), ('2','2'),('4','4'), ('8','8')], 'CPU Cores')
    eiqui_server_memoria = fields.Selection([('512MB','512MB'), ('1GB','1GB'),('2GB','2GB'), 
                                             ('4GB','4GB'), ('8GB','8GB'), ('16GB','16GB')], 'RAM')
    eiqui_server_disco = fields.Selection([('20GB','20GB'), ('30GB','30GB'),('40GB','40GB'), 
                                           ('60GB','60GB'), ('80GB','80GB'), ('160GB','160GB'),
                                           ('320GB','320GB'), ('480GB','480GB')], 'Disco')
    eiqui_server_tipo = fields.Selection([('S','S'), ('M','M'),('L','L')], 'Tipo')
    eiqui_server_so = fields.Many2one('eiqui.server.so', 'Sistema Operativo')
    eiqui_server_imagen_id = fields.Many2one('eiqui.server.imagen', 'Imagen Base')
    eiqui_server_fecha_inst = fields.Date('Fecha Instalación')
    eiqui_server_cambio_ids = fields.One2many('eiqui.server.cambio',
                                              'eiqui_server_id', 'Lista de Cambios', ondelete='cascade')
    eiqui_server_cliente_ids = fields.Many2many('res.partner', string='Lista de Clientes')
    
    product_category_name = fields.Char(compute='_get_product_category_name')
    
    def _get_product_category_name(self):
        #pydevd.settrace('10.0.3.1')
        category_name = self.categ_id.name
        self.product_category_name = category_name
    
    @api.constrains('eiqui_server_ip_privada', 'eiqui_server_ip_publica')
    def _check_ips(self):        
        p = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
        if self.eiqui_server_ip_privada and p.match(self.eiqui_server_ip_privada) is None:
            raise exceptions.ValidationError('Dirección IP privada inválida')
        if self.eiqui_server_ip_publica and p.match(self.eiqui_server_ip_publica) is None:
            raise exceptions.ValidationError('Dirección IP pública inválida')
    
product_server()

class cliente(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    
    #fields    
    eiqui_server_website = fields.Char('Eiqui Website', size=80)
    eiqui_server_ids = fields.Many2many('product.template', string='Lista de Servidores')
    
cliente()

class cambio(models.Model):
    _name = 'eiqui.server.cambio'    
    
    #fields
    eiqui_server_id = fields.Many2one('product.template')
    fecha = fields.Date('Fecha del Cambio')
    descripcion = fields.Text('Descripción')
    
cambio()

class imagen(models.Model):
    _name = 'eiqui.server.imagen'    
    
    #fields
    name = fields.Char('Nombre de Imagen', size=50)
    descripcion = fields.Text('Descripción')
    
cambio()

class so(models.Model):
    _name = 'eiqui.server.so'    
    
    #fields
    name = fields.Char('Sistema Operativo')
    descripcion = fields.Text('Descripción')
    
cambio()