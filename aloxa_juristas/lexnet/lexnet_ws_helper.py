# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from lib import xml2json
import ConfigParser
import json
import importlib

WSDLSettings = None
ModSettings = None
'''

Funcion que recupera los parametros del Modulo Lexnet de mod_settings.cfg
'''
def loadModSettings():
    global ModSettings
    if ModSettings is None:
        ModSettings = dict()
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read("mod_settings.cfg")
        section = 'Provider'        
        providerOpts = {'wsprovider_module':config.get(section, 'wsprovider_module'),
                        'wsprovider_class':config.get(section, 'wsprovider_class')}
        ModSettings[section] = providerOpts
    return ModSettings

'''
Funcion que recupera los parametros de WebServices Lexnet de ws_settings.cfg
y los introduce en el diccionario WSDLSettings
El diccionario WSDLSettings almacena la informacion del modo siguiente:
[Nombre_WebService]->Objeto LexnetWS con datos del WebServices asociado
'''
def loadWSDLSettings():
    global WSDLSettings
    if WSDLSettings is None:
        WSDLSettings = dict()
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read("ws_settings.cfg")
        sections = config.sections()
        for sec in sections:
            options = config.options(sec)
            lws = LexnetWS()
            for opt in options:
                optVal = config.get(sec, opt)
                setattr(lws, opt, optVal)
            WSDLSettings[sec] = lws
    return WSDLSettings

'''
Obtiene una instancia del objeto Adapter que permite acceder a los WebServices
mediante una implementacion especifica, por ejemplo al interfaz WSDL de Lexnet
Este mecanismo permite modificar las estrategias de implementacion
'''
def getWSProvider():
    loadModSettings()
    global ModSettings
    providerDict = ModSettings['Provider']
    providerModuleName = providerDict['wsprovider_module']
    providerClassName = providerDict['wsprovider_class']
    providerModule = importlib.import_module(providerModuleName)
    obProvider = getattr(providerModule, providerClassName)()

    return obProvider

'''
Modelo que encapsula la definicion de un WebService de Lexnet
'''
class LexnetWS(object):
    @property
    def url(self):
        return self._url
    @property
    def request(self):
        return self._request
    @property
    def requestXml(self):
        return xml2json.json2xml(json.loads(self._request))        
    @url.setter
    def url(self, url):
        self._url = url
    @request.setter
    def request(self, request):
        self._request = request
        
    def __init__(self, **kwargs):
        for kwarg in kwargs:
            setattr(self, kwarg, kwargs[kwarg])
            
