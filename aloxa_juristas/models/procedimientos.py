# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd

# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api, _
from openerp.osv import osv
#from openerp.exceptions import Warning
from datetime import datetime, timedelta

class expedientes_project(models.Model):
    _name = 'project.project'
    _inherit = 'project.project'
    
    '''
    Metodo que genera una factura borrado asociada al proyecto
    Genera una linea para cada tarea del mismo
    '''
    @api.one
    def generar_factura_proyecto(self):
        ai_model = self.env['account.invoice']
        ail_model = self.env['account.invoice.line']
        #pydevd.settrace("10.0.3.1")
        #Para cada tarea del proyecto creamos una linea de factura
        if self.partner_id:
            partner = self.partner_id
            do = self.env['juristas.opciones'].search([('nombre','=','Default')])            
            if do and do.account_id and do.journal_id:                    
                company = self.env.user.company_id
                currency = self.env.user.company_id.currency_id
                ai = ai_model.create({'partner_id':partner.id,
                                      'account_id':do.account_id.id,
                                      'company_id':company.id,
                                      'currency_id':currency.id,
                                      'journal_id':do.journal_id.id, #Diario de Ventas
                                      'reference_type':'none'})
                lines = []
                for task in self.task_ids:
                    line = ail_model.create({'account_id':ai.account_id.id,
                                             'name':task.name,
                                             'price_unit':0,
                                             'quantity':1})
                    lines.append(line.id)
                
                ai.invoice_line = lines
                self.factura_id = ai
            else:
                raise osv.except_osv(('Falta información'), ('Faltan Opciones en Configuración->Juristas->Opciones...'))
        else:
            raise osv.except_osv(('Falta información'), ('No ha especificado Cliente para el Proyecto...'))
            '''
            La forma recomendada de enviar un mensame emergente es la que sigue,
            sin embargo no funciona desde metodos invocados en buttons
            '''
            #===================================================================
            # return {'warning': {
            #         #===========================================================
            #         # 'title': _('Falta información'),
            #         # 'message': _('No ha especificado Cliente para el Proyecto...'),
            #         #===========================================================
            #         'title':'Falta información',
            #         'message': 'No ha especificado Cliente para el Proyecto...',
            #         }}
            #===================================================================
        
    
    #Fields
    partner_ids = fields.Many2many('res.partner', string='Colaboradores')
    partner_contrario_ids = fields.Many2many('res.partner', 'project_res_partner_contrario_rel',
                                             'project_id', 'partner_id', string='Contrarios')
    ambito_id = fields.Many2one('juristas.ambito', 'Ámbito')
    ambito_ruta = fields.Char('Ruta completa de Ámbitos', related='ambito_id.ambito_ruta')
    factura_id = fields.Many2one('account.invoice', 'Factura')
    turno = fields.Boolean('Turno de Oficio')
    versus_id = fields.Many2one('res.partner', 'Versus')
    #formulario_ids = fields.One2many('juristas.formulario', compute='_get_formularios') 
    
expedientes_project()

class expedientes_task(models.Model):
    _name = 'project.task'
    _inherit = 'project.task'   
    
    '''
    Calcula la fecha_limite calculada a partir de la fecha_notifi y del plazo
    teniendo en cuenta que los sabados (5) y domingos (6) son no habiles
    '''
    @api.depends('plazo', 'fecha_notifi')
    def _calcular_fecha_limite(self):
        #pydevd.settrace("10.0.3.1")
        if self.plazo and self.fecha_notifi:
            auxdate = datetime.strptime(self.fecha_notifi, '%Y-%m-%d')
            count = 1
            while count <= self.plazo:
                if auxdate.weekday() not in (5,6):
                    count += 1
                auxdate = auxdate + timedelta(days=1)
            self.fecha_limite = auxdate    
    
    @api.multi
    def adjuntar_archivo(self):
        #pydevd.settrace("10.0.3.1")
        if 'attach_id' in self.env.context:
            iamodel = self.env['ir.attachment']
            #Recuperamos el adjunto de la tarea
            attach_id = self.env.context['attach_id']
            attach = iamodel.search([('id','=',attach_id)])
            if attach:                
                values = dict()
                #Copiamos los valores del adjunto original
                for key in attach._fields:
                    values[key] = eval('attach.' + key)
                #Eliminamos los campos que no queremos usar para crear el nuevo adjunto
                for key in ['id', 'partner_id', 'res_name', 'user_id', 'company_id',
                            'create_uid', 'parent_id']:
                    del values[key]                      
                #pydevd.settrace('10.0.3.1')
                #Pasamos los valores especificos para el adjunto
                values['res_model'] = 'project.task'
                values['res_id'] = self.id           
                
                iamodel.create(values)
                
    @api.multi
    def crear_evento(self, *args):
        #pydevd.settrace('10.0.3.1')        
        '''
        Invocar a un form de creacion de eventos de calendario
        '''
        return  {
            'type': 'ir.actions.act_window',
            'name': 'Crear Evento',            
            #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
            'res_model': 'calendar.event',
            'context': {'task_id': self.id},                            
            'view_type': 'form',
            'view_mode': 'calendar,form,tree',            
            'target': 'current',               
            'nodestroy': True,
        }
        
    @api.multi
    def notificar_adjunto(self):
        #pydevd.settrace('10.0.3.1')

        '''
        Invocar a un form de creacion de notificaciones de tarea
        '''
        return  {
            'type': 'ir.actions.act_window',
            'name': 'Crear Notificacion',            
            #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
            'res_model': 'juristas.notificacion.tarea',
            'context': {'task_id': self.id},                            
            'view_type': 'form',
            'view_mode': 'form',            
            'target': 'new',               
            'nodestroy': True,
        }
    
    '''
    Sobreescritura del write para controlar los cambios en la fecha_juicio
    Si ese campo cambia hay que programar un evento de calendario con alerta
    con los dias de antelacion indicados en el parametro de opciones correspondiente
    '''
    @api.multi
    def write(self, values):        
        task = super(expedientes_task, self).write(values)
        if 'fecha_juicio' in values:            
            #pydevd.settrace('10.0.3.1')
            #Tomo la opcion que indica el numero de dias para el recordatorio
            opcion_def = self.env['juristas.opciones'].search([('nombre','=','Default')])
            dias = opcion_def.dias_aviso_juicio
            if not dias:
                dias = 7        
            
            #Borramos posible aviso de calendario anterior asociados a la oportunidad
            #=======================================================================
            # for event in cal_model.search([('opportunity_id','=',self.id)]):
            #     event.unlink()
            #=======================================================================
            #Creamos el nuevo aviso en el calendario
            cal_event = self.env['calendar.event'].create({                   
                    'name':'JUICIO: ' + self.name + ', ' + self.project_id.name,                                                                               
                    'state' : 'open',
                    'message_unread': True,
                    'start' : self.fecha_juicio,
                    'stop' : self.fecha_juicio,
                    })                                               
            #Establecimiento de la alarma de aviso asociada al evento del calendario        
            aviso = self.env['calendar.alarm'].search([('duration','=',dias),('interval','=','days')])
            if not aviso:
                aviso = self.env['calendar.alarm'].create({'duration':dias, 'interval':'days',        
                                                           'name':str(dias) + ' días', 'type':'notification'})
            cal_event.alarm_ids = aviso
            #Asociamos el evento con la tarea
            cal_event.task_id = self.id
            
        return task       
        
    #Fields
    formulario_ids = fields.Many2many('juristas.formulario', string='Formularios')
    organo_id = fields.Many2one('juristas.organo', 'Órgano')
    organo_ruta = fields.Char('Ruta completa de Órgano', related='organo_id.organo_ruta')
    '''
    Campo calculado que reemplazara a date_deadline (Fecha Limite de la Tarea)
    cuando se especifique el plazo y la fecha_notifi
    '''
    fecha_limite = fields.Date(compute=_calcular_fecha_limite)
    fecha_notifi = fields.Date('Fecha Notificación')
    fecha_juicio = fields.Datetime('Fecha Juicio')    
    plazo = fields.Integer('Plazo (días)')    
    adjunto_ids = fields.One2many('ir.attachment', 'res_id', 'Adjuntos', domain=[('res_model','!=',False)])
    evento_ids = fields.One2many('calendar.event', 'task_id', string='Eventos')
    notificacion_tarea_ids = fields.One2many('juristas.notificacion.tarea', 'task_id',
                                             string='Notificaciones')
        
expedientes_task()

class notificacion_tarea(models.Model):
    _name='juristas.notificacion.tarea'
    
    def default_task_id(self):
        #pydevd.settrace("10.0.3.1")
        if 'task_id' in self.env.context:
            task = self.env['project.task'].search([('id','=',self.env.context['task_id'])])
            return task
        return False
    
    @api.multi
    def enviar_email_notificacion(self):
        #self.mail = True
        return  {
            'type': 'ir.actions.act_window',
            'name': 'Enviar Notificacion via Mail',            
            #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
            'res_model': 'mail.mail',
            'context': {'notify_id': self.id},                            
            'view_type': 'form',
            'view_mode': 'form',            
            'target': 'new',               
            'nodestroy': True,
            }        
        #TODO
        #Meter logica de envio de email
    
    @api.multi
    def enviar_carta_notificacion(self):
        self.carta = True
        
    @api.multi
    def guardar_notificacion(self):
        #pydevd.settrace('10.0.3.1')
        values = {'task_id':self.task_id.id,
                  'adjunto_id':self.adjunto_id.id if self.adjunto_id else None,
                  'partner_id':self.partner_id.id if self.partner_id else None,
                  'texto':self.texto
                  }
        self.write(values)
        
    
    #===========================================================================
    # def get_partner_lista(self):
    #     #pydevd.settrace("10.0.3.1")
    #     if 'task_id' in self.env.context:
    #         task = self.env['project.task'].search([('id','=',self.env.context['task_id'])])
    #         lista = list()
    #         if task and task.project_id:
    #             if task.project_id.partner_id:
    #                 lista.append(task.project_id.partner_id.id)
    #             if task.project_id.partner_ids:
    #                 for p in task.project_id.partner_ids:
    #                     lista.append(p.id)
    #         return str(tuple(lista))
    #===========================================================================
    
    #Fields
    #task_id = fields.Many2one('project.task', default=default_task_id, readonly=True, required=True)
    task_id = fields.Many2one('project.task', default=default_task_id, string="Trámite")
    mail = fields.Boolean('Mail', default=False)
    carta = fields.Boolean('Carta', default=False)
    texto = fields.Text('Texto Mensaje')
    #partner_lista = fields.Char(compute='get_partner_lista')
    #partner_lista = fields.Char(default=get_partner_lista)
    partner_id = fields.Many2one('res.partner', 'Receptor')
    adjunto_id = fields.Many2one('ir.attachment', 'Adjunto')
    #colabor_ids = fields.Many2many(related='task_id.project_id.partner_ids')
    
notificacion_tarea()
