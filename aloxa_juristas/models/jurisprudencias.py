# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api
from datetime import date
#from datetime import datetime, timedelta

class jurisprudencia(models.Model):
    _name = 'juristas.jurisprudencia'
    _rec_name='numero'   
    
    #Fields
    numero=fields.Char('Número de Sentencia', size=100)
    #Por aqui se puede usar la busqueda textual
    descripcion=fields.Text('Descripción')
    jurisdiccion=fields.Selection([('Internacional', 'Internacional'), ('Constitucional', 'Constitucional'),
                             ('Civil', 'Civil'), ('Civil y Mercantil', 'Civil y Mercantil'), ('Social', 'Social'), ('Contencioso Administrativo', 'Contencioso Administrativo'),
                             ('Penal', 'Penal'), ('Otros', 'Otros')], 'Jurisdicción')
    origen_id=fields.Many2one('juristas.organo', 'Origen')
    origen_ruta = fields.Char('Ruta completa de Origen', related='origen_id.organo_ruta')
    fallo=fields.Selection([('Estima', 'Estima'), ('Estima Parcialmente', 'Estima Parcialmente'),
                             ('Desestima', 'Desestima'), ('Condena', 'Condena'), ('Absuelve', 'Absuelve'),
                             ('Inadmite', 'Inadmite'), ('Archiva', 'Archiva'), ('Anula', 'Anula'),
                             ('No Anula', 'No Anula')], 'Fallo')
    num_recurso=fields.Char('Número de Recurso', size=100)    
    tipo_resolucion=fields.Selection([('Sentencia', 'Sentencia'), ('Auto', 'Auto'),
                             ('Acuerdo', 'Acuerdo'), ('Resolucion', 'Resolución'), ('Declaracion', 'Declaración'),
                             ('Decreto', 'Decreto')], 'Tipo de Resolución')    
    fecha=fields.Date('Fecha')    
    seccion=fields.Selection([('Unica', 'Única'), (u'1ª', '1ª'), (u'2ª', '2ª'), (u'3ª', '3ª'),
                             (u'4ª', '4ª'), (u'5ª', '5ª'), (u'6ª', '6ª')], 'Sección')
    ponente=fields.Char('Ponente', size=255)    
    #tag_ids=fields.Many2many('juristas.tag', 'jurisprudencia_id', 'tag_id', string="Etiquetas")
    tag_ids=fields.Many2many('project.category', string="Etiquetas")    
    
jurisprudencia()
