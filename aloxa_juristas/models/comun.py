# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api
from openerp.osv import osv
from datetime import date

class organo(models.Model):
    _name='juristas.organo'
    _rec_name='nombre'
    
    def _get_organo_path(self):
        for rec in self:     
            organos_list=[]
            organos_path=""
            
            #pydevd.settrace("10.0.3.1")            
            organos_list.append(rec.nombre)
            parent = rec.parent_id
            while parent:                
                organos_list.append(parent.nombre)
                if parent.parent_id:
                    if parent.nombre != parent.parent_id.nombre:
                        parent = parent.parent_id
                    else:
                        parent = None
                else:
                    parent = None
            if organos_list:                    
                organos_list.reverse()
                for organo in organos_list:
                    organos_path += organo + '/'                
            organos_path = organos_path[:-1]
    
            rec.organo_ruta = organos_path
    
    nombre=fields.Char('Nombre', required=True, size=100)
    parent_id=fields.Many2one('juristas.organo', 'Padre')
    organo_ruta=fields.Char('Ruta completa del Órgano', compute='_get_organo_path')
    
organo()

#===============================================================================
# class etiqueta(models.Model):
#     _name = 'juristas.tag'
#     _inherit = 'res.partner.category'
#      
# etiqueta()
#===============================================================================

class ambito(models.Model):
    _name='juristas.ambito'
    _rec_name='nombre'
    
    def _get_ambito_path(self):
        for rec in self:     
            ambitos_list=[]
            ambitos_path=""
            
            #pydevd.settrace("10.0.3.1")            
            ambitos_list.append(rec.nombre)
            parent = rec.parent_id
            while parent:                
                ambitos_list.append(parent.nombre)
                if parent.parent_id:
                    if parent.nombre != parent.parent_id.nombre:
                        parent = parent.parent_id
                    else:
                        parent = None
                else:
                    parent = None
            if ambitos_list:                    
                ambitos_list.reverse()
                for ambito in ambitos_list:
                    ambitos_path += ambito + '/'                
            ambitos_path = ambitos_path[:-1]
    
            rec.ambito_ruta = ambitos_path
    
    #Fields
    nombre=fields.Char('Nombre', size=100, required=True)
    parent_id=fields.Many2one('juristas.ambito', 'Padre')
    ambito_ruta=fields.Char('Ruta completa de Ámbitos', compute='_get_ambito_path')

ambito()

class opciones(models.Model):
    _name = 'juristas.opciones'
    _rec_name = 'nombre'
    #Fields
    nombre=fields.Char('Nombre', default=lambda self: "Default",)
    account_id=fields.Many2one('account.account', 'Cuenta Contable para Proyectos Facturados')   
    journal_id=fields.Many2one('account.journal', 'Diario para Proyectos Facturados')
    dias_aviso_juicio=fields.Integer('Antelación Aviso Juicio (días)', default=7)   
    
opciones()

class adjunto(models.Model):
    _name = 'ir.attachment'
    _inherit = 'ir.attachment'
    
    '''
    Metodo que devuelve una action que invoca al form de creacion de formulario con el
    valor self.id e el contexto para identificar al adjunto que se asociara al mismo
    '''
    @api.multi
    def crear_formulario(self):
        #pydevd.settrace("10.0.3.1")
        if self.formulario_id:
            raise osv.except_osv(('No se puede crear el Formulario'),
                                 ('Ya existe un Formulario para ese Adjunto'))            
        '''
        Meter en contexto el self.id (id del adjunto) e invocar al form de creacion
        de formularios
        '''
        return  {
            'type': 'ir.actions.act_window',
            'name': 'Crear Formulario de Adjunto',            
            #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
            'res_model': 'juristas.formulario',
            'context': {'attach_id': self.id},                            
            'view_type': 'form',
            'view_mode': 'form',            
            'target': 'current',               
            'nodestroy': True,
        }

    '''
    Metodo que devuelve una action que invoca al tree de tareas con el
    valor self.id en el contexto para identificar al adjunto que se asociara a la tarea
    Tambien filtra aquellas tareas que tienen asociado como colaborador el emisor del mensaje
    del archivo adjunto
    '''    
    @api.multi  
    def adjuntar_task(self):
        if self.task_id:
            raise osv.except_osv(('No se puede adjuntar el archivo'),
                                 ('El archivo ya ha sido adjuntado'))            
        #pydevd.settrace("192.168.3.1")
        #view_id de la vista tree de tareas para adjuntar el archivo
        view_id = self.env['ir.ui.view'].search([('name','=','Tree Tareas Adjuntar Archivo')])
        #Emisor del mensaje
        query = 'SELECT message_id FROM message_attachment_rel WHERE attachment_id='+str(self.id)
        self.env.cr.execute(query)
        res = self.env.cr.fetchall()
        emisor=None
        if res:
            message_id = res[0][0]
            message = self.env['mail.message'].search([('id','=',message_id)])
            emisor = message.partner_from_id        
        if emisor:        
            tareas = []        
            '''
            Tomamos las tareas en estado que no sea "Realizado" ni "Cancelado"
            '''
            tipos_activas = self.env['project.task.type'].search([('name','not in',['Realizado','Cancelado'])])
            for tipo in tipos_activas:
                tareas_tipo = self.env['project.task'].search([('stage_id','=',tipo.id)])
                tareas += tareas_tipo
            
            '''
            De las tareas de la lista anterior tomamos aquellas cuyo proyecto asociado tenga como 
            colaborador el emisor del mensaje
            '''
            tareas_filtrar = []  
            for tarea in tareas:
                proyecto = tarea.project_id
                if proyecto:
                    colaboradores = proyecto.partner_ids
                    if colaboradores:
                        for colaborador in colaboradores:
                            if colaborador.name == emisor.name:
                                tareas_filtrar.append(tarea)
                                break          
        else:
            '''
            Si el emisor no esta definido tomamos todas las tareas
            '''
            tareas_filtrar = self.env['project.task'].search([])
            
        tarea_ids = [tarea.id for tarea in tareas_filtrar]
        #pydevd.settrace("192.168.3.1")
        if view_id:            
            return  {
                'type': 'ir.actions.act_window',
                'name': 'Adjuntar Archivo',            
                'domain': [('id','in',tarea_ids)],            
                'res_model': 'project.task',
                'context': {'attach_id': self.id},                      
                'view_type': 'form',
                'view_mode': 'tree',
                'view_id': view_id.id,                     
                'target': 'new',               
                'nodestroy': True,
            }
    
    def get_formulario(self):
        #pydevd.settrace("10.0.3.1")
        for rec in self:
            adjunto = rec.search([('res_model','=','juristas.formulario'),
                                   ('name','=',rec.name)])
            if adjunto:
                adjunto = adjunto[0]
                form = rec.env['juristas.formulario'].search([('id','=',adjunto.res_id)])
                if form:
                    rec.formulario_id = form.id
                    
    def get_task(self):
        for rec in self:
            adjunto = rec.search([('res_model','=','project.task'),
                                   ('name','=',rec.name)])
            if adjunto:
                adjunto = adjunto[0]
                task = rec.env['project.task'].search([('id','=',adjunto.res_id)])
                if task:
                    rec.task_id = task.id
        
    formulario_id = fields.Many2one('juristas.formulario', string="Formulario Asociado", compute=get_formulario)
    task_id = fields.Many2one('project.task', string="Trámite Asociado", compute=get_task)

adjunto()

class calendar_event(models.Model):
    _name='calendar.event'
    _inherit='calendar.event'
    
    '''
    Sobreescribimos el metodo create para implementar la posibilidad de asociar el evento
    a una tarea
    '''
    @api.model
    def create(self, values):
        #pydevd.settrace('10.0.3.1')   
        event = super(calendar_event, self).create(values)
        '''
        Si en el context hay una entrada task_id indicara que se ha invocado al form
        de creacion del evento desde la seccion Crer Alerta de la Tarea, lo cual
        permite crear la alerta asociada a la misma 
        '''
        if 'task_id' in self.env.context:
            ptmodel = self.env['project.task']
            #Recuperamos el id de la tarea
            task_id = self.env.context['task_id']
            task = ptmodel.search([('id','=',task_id)])
            #Asociamos el evento con la tarea
            if task:
                event.task_id = task.id                                   

        return event
    
    #Fields
    task_id = fields.Many2one('project.task', 'Trámite')
    
calendar_event()

class res_partner(models.Model):
    _name='res.partner'
    _inherit='res.partner'
    
    '''
    default_get para meter valores por defecto en el modelo en funcion de variables del contexto
    '''
    @api.model
    def default_get(self, fields):
        data = super(res_partner, self).default_get(fields)
        if 'search_default_abogados' in self.env.context:
            data['rol_trabajo'] = ('Abogado','Abogado')
        elif 'search_default_procuradores' in self.env.context:
            data['rol_trabajo'] = ('Procurador','Procurador')
        return data
    
    #Fields
    rol_trabajo = fields.Selection([('Procurador','Procurador'),('Abogado','Abogado')],
                                    string='Rol de Trabajo')    
    
res_partner()