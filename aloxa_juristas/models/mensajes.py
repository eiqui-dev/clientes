# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api, _
from openerp.osv import osv
import re
#from openerp.exceptions import Warning
from datetime import datetime, timedelta

class mensaje(models.Model):
    _name = 'mail.message'
    _inherit = 'mail.message'
    
    '''
    Sobreescritura del metodo create para asignar valores por defecto a los mensajes
    entrantes procedentes del mail configurado como entrante, que tendran los campos
    model: False
    res_id: 0
    Estos valores hacen que el registro no se pueda borrar, para solucionarlo asignamos
    valores los valores a los campos
    model: 'res.partner'
    res_id: 1
    (Estos valores corresponden al registro de la compañia que siempre existira)
    Con lo cual ya podremos eliminar esos registros
    '''
    @api.model
    def create(self, values):
        #pydevd.settrace('10.0.3.1')
        message = super(mensaje, self).create(values)
        if not message.model and message.res_id == 0:
            message.model = 'res.partner'
            message.res_id = 1
        return message
    
    #===========================================================================
    # @api.multi
    # def responder_mensaje(self):
    #     return  {
    #         'type': 'ir.actions.act_window',
    #         'name': 'Responder Mensaje',            
    #         #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
    #         'res_model': 'mail.compose.message',
    #         #'context': {'attach_id': self.id},                            
    #         'view_type': 'form',
    #         'view_mode': 'form',            
    #         'target': 'new',               
    #         'nodestroy': True,
    #     }
    #===========================================================================
        
    @api.multi
    def enviar_mail(self):
        return  {
            'type': 'ir.actions.act_window',
            'name': 'Responder Mensaje via Mail',            
            #'domain': ['&', ('res_model','=','project.task'), ('res_id','=',context['task_id'])],            
            'res_model': 'mail.mail',
            #'context': {'attach_id': self.id},                            
            'view_type': 'form',
            'view_mode': 'form',            
            'target': 'new',               
            'nodestroy': True,
        }
        
    #===========================================================================
    # @api.multi
    # def ver_hilo_mensaje(self):
    #     if 'message_id' in self.env.context:
    #         message_id = self.env.context['message_id']
    #         uid = self.env.user.id
    #     return  {
    #         'type': 'ir.actions.client',
    #         'name': 'Hilo del Mensaje',
    #         'tag': 'mail.wall',
    #         'res_model': 'mail.message',
    #         'context': {
    #           'default_model': 'res.users',
    #           'default_res_id': uid,
    #           'thread_model': 'res.partner',
    #           'search_default_message_unread': True,
    #           'needaction_menu_ref': ['mail.mail_starfeeds', 'mail.mail_inboxfeeds']
    #         },
    #         'params': {'domain': [('partner_ids.user_ids', 'in', [uid]),
    #                               ('id', '=', message_id)],
    #                    'view_mailbox': True,
    #                    'read_action': 'read',
    #                    'show_compose_message': False
    #                    },
    #     }
    #===========================================================================
    
    '''
    Metodo que genera obtiene el res.partner a partir de la direccion de mail del emisor
    del mensaje.
    El campo email_from se registra de dos maneras en el mail.message:
    1.- Nombre <mail@sample.com>
    2.- mail@example.com
    Hay que determinar el formato para extraer en el caso 1 la parte correspondiente al email
    '''    
    def get_partner_from(self):
        for rec in self:
            mail_from = self._get_mail_email_from(rec)
            #Una vez tenemos el email buscamos el cliente
            #pydevd.settrace('10.0.3.1')
            partner = self.env['res.partner'].search([('email','=',mail_from)])
            if partner:               
                rec.partner_from_id = partner[0].id
    
    #===========================================================================
    # @api.depends('partner_from_id')             
    # def get_partner_from_email(self):
    #     for rec in self:
    #         mail_from = self._get_mail_email_from(rec)
    #         self.partner_from_mail = mail_from
    #===========================================================================
    
    '''
    Obtiene la direccion de email del contacto pasado en el argumento
    '''     
    def _get_mail_email_from(self, rec):        
        rpmodel = self.env['res.partner']
        mail_from = rec.email_from
        #Buscamos el patron <mail@sample.com> en el campo email_from
        p = re.compile('(<[\w|(@|.|#|-|_|~|!|$|\(|\)|*|+|,|;)]+>)')
        ocu = p.findall(unicode(mail_from))
        if ocu:
            #Eliminamos el < inicial y el > final
            mail_from = ocu[0][1:-1]
        
        return mail_from
            
        

    @api.multi             
    def marcar_leido(self):
        self.set_message_read(True)

    @api.multi             
    def marcar_no_leido(self):
        self.set_message_read(False)
            
    #Fields
    #partner_from_mail = fields.Char('Remitente', compute=get_partner_from_email, store=True)
    partner_from_id = fields.Many2one('res.partner', string='Emisor',
                                      compute=get_partner_from)    
    #formulario_ids = fields.One2many('juristas.formulario', compute='_get_formularios') 
    
mensaje()

class mail_mail(models.Model):
    _name='mail.mail'
    _inherit='mail.mail'
    
    '''
    Metodo utilizado para cargar valores por defecto cuando este definido algun elemento en el contexto
    Usado para cargar los valores predefinidos en el formulario de envio de email
    '''
    @api.model
    def default_get(self, fields):
    #def default_get(self, cr, uid, fields, context=None):
        #pydevd.settrace("10.0.3.1")
        res = super(mail_mail, self).default_get(fields)
        subject = ''
        email_to = ''
        email_from = ''
        body_html = ''
        if 'message_id' in self.env.context:
            message = self.env['mail.message'].search([('id', '=', self.env.context['message_id'])])
            if message.subject:
                subject = 'RE: ' + unicode(message.subject)
            email_to = message.email_from
            res.update({'subject':subject,
                        'email_to':email_to
                        }) 
        #res = super(mail_mail, self).default_get(cr, uid, fields, context)
        if 'notify_id' in self.env.context:
            notify = self.env['juristas.notificacion.tarea'].search([('id', '=', self.env.context['notify_id'])])
            if notify:
                subject = 'Notificación de Trámite'
                if notify.partner_id:
                    email_to = notify.partner_id.email if notify.partner_id.email else ''
                email_from = self.env.user.company_id.email if self.env.user.company_id.email else ''
                if notify.texto:
                    body_html = notify.texto if notify.texto else ''
                res.update({'subject':subject,
                            'email_to':email_to,
                            'email_from':email_from,
                            'body_html':body_html,
                            'attachment_ids':[notify.adjunto_id.id] if notify.adjunto_id else None,
                            })
        return res
        
    @api.multi
    def enviar(self):
        #pydevd.settrace("10.0.3.1")
        success = super(mail_mail, self).send()
        if 'notify_id' in self.env.context:
            notify = self.env['juristas.notificacion.tarea'].search([('id', '=', self.env.context['notify_id'])])
            if notify:
                notify.mail = True          
        return success
        
mail_mail()
