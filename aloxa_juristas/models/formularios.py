# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api
from datetime import date
#from datetime import datetime, timedelta

class formulario(models.Model):
    _name = 'juristas.formulario'
    _rec_name='nombre'
    
    '''
    Sobreescribimos el metodo create para implementar la posibilidad de crear el formulario
    asociado a un adjunto desde el formulario de Tarea
    '''
    @api.model
    def create(self, values):        
        form = super(formulario, self).create(values)
        '''
        Si en el context hay una entrada attach_id indicara que se ha invocado al form
        de creacion de formulario desde la seccion Formularios del form de Tarea, lo cual
        permite crear un formulario asociado a un documento adjunto a la misma
        '''       
        if 'attach_id' in self.env.context:
            iamodel = self.env['ir.attachment']
            #Recuperamos el adjunto de la tarea
            attach_id = self.env.context['attach_id']
            attach = iamodel.search([('id','=',attach_id)])
            if attach:                
                values = dict()
                #Copiamos los valores del adjunto original
                for key in attach._fields:
                    values[key] = eval('attach.' + key)
                #Eliminamos los campos que no queremos usar para crear el nuevo adjunto
                for key in ['id', 'partner_id', 'res_name', 'user_id', 'company_id',
                            'create_uid', 'parent_id']:
                    del values[key]                      
                #pydevd.settrace('10.0.3.1')
                #Pasamos los valores especificos para el adjunto
                values['res_model'] = 'juristas.formulario'
                values['res_id'] = form.id           
                
                iamodel.create(values)

        return form
    
    #Fields
    nombre=fields.Char('Nombre', size=100)
    #Por aqui se puede usar la busqueda textual
    descripcion=fields.Text('Descripción')
    ambito_id=fields.Many2one('juristas.ambito', 'Ámbito')    
    ambito_ruta=fields.Char('Ruta completa de Ámbitos', related='ambito_id.ambito_ruta')
    emisor=fields.Selection([('Administracion', 'Administración'), ('Abogado', 'Abogado'),
                             ('Juez', 'Juez')], 'Emisor')
    fecha_alta=fields.Date(default=date.today().strftime('%Y-%m-%d'))
    #tag_ids=fields.Many2many('juristas.tag', 'formulario_id', 'tag_id', string="Etiquetas")
    tag_ids=fields.Many2many('project.category', string="Etiquetas") 
       
    
formulario() 
