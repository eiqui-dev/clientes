# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################


{
    "name": "Módulo de Gestión de Proyectos Jurídicos",
    "version": "0.1",
    "category": "Juristas",
    "icon": "/aloxa_juristas/static/src/img/icon.png",
    "depends": [
                'base',
                'sale',
                'project',
                'document',
                'calendar',
                #'account_payment_extension',
                'l10n_es_toponyms',
                #'account_analytic_analysis',
                ],
    "author": "Solucións Aloxa S.L.",
    "description": "Módulo de Gestión de Proyectos Jurídicos",
    "init_xml": [],
    'external_dependencies': {
        #'python': ['unidecode'],
    },
    "data": ['data/data.xml',
             'data/ir.model.data.csv',
             'data/ir.module.category.csv',
             'data/res.groups.csv',        
             'data/res.partner.csv', 
             'data/res.users.csv',                         
             'data/juristas.opciones.csv',
             'data/ir.ui.menu.csv',
             'data/juristas.ambito.csv',
             'data/juristas.organo.csv',
             'data/project.category.csv',
             'data/project.task.type.csv',
             'data/juristas.formulario.csv',
             'data/juristas.jurisprudencia.csv',
             'data/project.project.csv',
             'data/project.task.csv',             
             'security/ir.model.access.csv',
             'views/comun.xml',             
             'views/procedimientos.xml',
             'views/formularios.xml',
             'views/jurisprudencias.xml',
             'views/mensajes.xml',
             ],
    "demo": [],
    "installable": True,
    "active": False,
}
