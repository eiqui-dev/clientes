# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Solucións Aloxa S.L. <info@aloxa.eu>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#===============================================================================
# # REMOTE DEBUG
#import pydevd
# 
# # ...
# 
# # breakpoint
#pydevd.settrace("10.0.3.1")
#===============================================================================
from openerp import models, fields, api

'''
Modelo que sobreescribe sale.order.line
'''
class sale_order_line(models.Model):                       
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'
    
    #===========================================================================
    # @api.multi
    # def invoice_line_create(self):
    #     create_ids = super(sale_order_line, self).invoice_line_create()
    #     for id in create_ids:
    #         inv_line = self.env['account.invoice.line'].search([('id','=',id)])
    #     
    #     return create_ids
    #===========================================================================

    #Fields               
    num_orden = fields.Char('Num. Orden')
                      
sale_order_line()

'''
Modelo que sobreescribe account.invoice.line
'''
class account_invoice_line(models.Model):                       
    _name = 'account.invoice.line'
    _inherit = 'account.invoice.line'
    
    @api.multi
    def _get_num_orden(self):
        #pydevd.settrace("10.0.3.1")
        for res in self:
            if res.origin and res.product_id:
                #Localizamos el pedido
                saleo = self.env['sale.order'].search([('name','=',res.origin)])
                if saleo:
                    #Recorremos las lineas hasta localizar la del producto
                    for saleol in saleo.order_line:
                        if saleol.product_id.id == res.product_id.id:
                            res.num_orden = saleol.num_orden
                            return
                            
                res.num_orden = ''

    #Fields               
    num_orden = fields.Char('Num. Orden', compute=_get_num_orden)
                      
account_invoice_line()

'''
Modelo que sobreescribe stock.move (linea de albaran)
'''
class stock_move(models.Model):                       
    _name = 'stock.move'
    _inherit = 'stock.move'
    
    @api.multi
    def _get_num_orden(self):
        for res in self:
            if res.origin and res.product_id:
                #Localizamos el pedido
                saleo = self.env['sale.order'].search([('name','=',res.origin)])
                if saleo:
                    #Recorremos las lineas hasta localizar la del producto
                    for saleol in saleo.order_line:
                        if saleol.product_id.id == res.product_id.id:
                            res.num_orden = saleol.num_orden
                            return
                            
                res.num_orden = ''

    #Fields               
    num_orden = fields.Char('Num. Orden', compute=_get_num_orden)
                      
stock_move()
